<?php
    $DB_NAME = "gudang";
    $DB_USER = "root";
    $DB_PASS =  "";
    $DB_SERVER_LOC = "localhost";

    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        $conn = mysqli_connect($DB_SERVER_LOC,$DB_USER,$DB_PASS,$DB_NAME);
        $sql = "SELECT nama_suplier FROM suplier ORDER BY nama_suplier asc";
        $result = mysqli_query($conn,$sql);
        if(mysqli_num_rows($result)>0){
            header("Access-Control-Allow-Origin: *");
            header("Content-type: application/json; charset=UTF-8");

            $nama_suplier = array();
            while($nm_nama_suplier = mysqli_fetch_assoc($result)){
                array_push($nama_suplier,$nm_nama_suplier);
            }
            echo json_encode($nama_suplier);
        }
    }
?>