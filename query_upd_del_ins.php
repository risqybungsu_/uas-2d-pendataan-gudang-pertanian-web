<?php
    $DB_NAME = "gudang";
    $DB_USER = "root";
    $DB_PASS =  "";
    $DB_SERVER_LOC = "localhost";

    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        $conn = mysqli_connect($DB_SERVER_LOC,$DB_USER,$DB_PASS,$DB_NAME);
        $mode = $_POST['mode'];
        $respon = array(); $respon['kode'] = '000';
        switch($mode){
            case "insert":
                $kode = $_POST["kode"];
                $nama = mysqli_real_escape_string($conn,trim($_POST["nama"]));
                $nama_jenis = $_POST["nama_jenis"];
                $nama_suplier = $_POST["nama_suplier"];
                $imstr = $_POST["image"];
                $file = $_POST["file"];
				$alamat = $_POST["alamat"];
                $path = "images/";

                $sql = "SELECT id_jenis from jenis where nama_jenis='$nama_jenis'";
                $sql1 = "SELECT id_suplier from suplier where nama_suplier='$nama_suplier'";

                $result = mysqli_query($conn,$sql);

                $result1 = mysqli_query($conn,$sql1);

                if (mysqli_num_rows($result)>0 && mysqli_num_rows($result1)>0) {
                    $data = mysqli_fetch_assoc($result);
                    $data1 = mysqli_fetch_assoc($result1);
                    $id_jenis = $data['id_jenis'];
                    $id_suplier = $data1['id_suplier'];

                    $sql = "INSERT into barang(kode, nama, id_jenis, photos, alamat, id_suplier) values(
                        '$kode','$nama','$id_jenis','$file', '$alamat', '$id_suplier')";
                    $result = mysqli_query($conn,$sql);
                    if($result){
                        if(file_put_contents($path.$file,base64_decode($imstr)) == false){
                            $sql = "delete from barang where kode='$kode'";
                            mysqli_query($conn,$sql);
                            $respon['kode'] = "111";
                            echo json_encode($respon); exit();    
                        }else{
                            echo json_encode($respon); exit(); //insert data sukses semua
                        }
                    }else{
                        $respon['kode'] = "111";
                        echo json_encode($respon); exit();
                    }
                }
            break;
            case "update":
                $kode = $_POST["kode"];
                $nama = mysqli_real_escape_string($conn,trim($_POST["nama"]));
                $nama_jenis = $_POST["nama_jenis"];
                $imstr = $_POST["image"];
                $file = $_POST["file"];
				$alamat = $_POST["alamat"];
                $path = "images/";
                $nama_suplier = $_POST["nama_suplier"];


                $sql = "SELECT id_jenis from jenis where nama_jenis='$nama_jenis'";
                $sql1 = "SELECT id_suplier from suplier where nama_suplier='$nama_suplier'";

                $result = mysqli_query($conn,$sql);
                $result1 = mysqli_query($conn,$sql1);

                if (mysqli_num_rows($result)>0 && mysqli_num_rows($result1)>0) {
                    $data = mysqli_fetch_assoc($result);
                    $data1 = mysqli_fetch_assoc($result1);
                    $id_jenis = $data['id_jenis'];
                    $id_suplier = $data1['id_suplier'];

                    $sql = "";
                    if($imstr==""){
                        $sql = "UPDATE barang SET nama='$nama',id_jenis=$id_jenis, alamat='$alamat', id_suplier='$id_suplier'
                        where kode='$kode'";
                        $result = mysqli_query($conn,$sql);
                        if($result){
                            echo json_encode($respon); exit();
                        }else{
                            $respon['kode'] = "111";
                            echo json_encode($respon); exit();
                        }
                    }else{
                        if(file_put_contents($path.$file,base64_decode($imstr)) == false){
                            $respon['kode'] = "111";
                            echo json_encode($respon); exit();    
                        }else{
                            $sql = "UPDATE barang SET nama='$nama',id_jenis=$id_jenis,photos='$file', alamat='$alamat', id_suplier='$id_suplier'
                                    where kode='$kode'";
                            $result = mysqli_query($conn,$sql);
                            if($result){
                                echo json_encode($respon); exit(); //update data sukses semua
                            }else{
                                $respon['kode'] = "111";
                                echo json_encode($respon); exit();
                            }
                        }
                    }
                }
            break;
            case "delete":
                $kode = $_POST["kode"];
                $sql = "SELECT photos from barang where kode='$kode'";
                $result = mysqli_query($conn,$sql);
                if($result){
                    if(mysqli_num_rows($result)>0){
                        $data = mysqli_fetch_assoc($result);
                        $photos = $data['photos'];
                        $path = "images/";
                        unlink($path.$photos);
                    }
                    $sql = "DELETE from barang where kode='$kode'";
                    $result = mysqli_query($conn,$sql);
                    if($result){
                        echo json_encode($respon); exit(); //delete data sukses
                    }else{
                        $respon['kode'] = "111";
                        echo json_encode($respon); exit();
                    }
                }
            break;
        }
    }
?>