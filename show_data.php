<?php
    $DB_NAME = "gudang";
    $DB_USER = "root";
    $DB_PASS =  "";
    $DB_SERVER_LOC = "localhost";

    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        $conn = mysqli_connect($DB_SERVER_LOC,$DB_USER,$DB_PASS,$DB_NAME);
        $sql = "SELECT m.kode,m.nama,p.nama_jenis,concat('http://192.168.42.207/datagudang/images/',photos) as url, m.alamat,j.nama_suplier
        FROM barang m,jenis p , suplier j
        WHERE m.id_jenis=p.id_jenis AND m.id_suplier = j.id_suplier";
        $result = mysqli_query($conn,$sql);
        if(mysqli_num_rows($result)>0){
            header("Access-Control-Allow-Origin: *");
            header("Content-type: application/json; charset=UTF-8");

            $data_mhs = array();
            while($mhs = mysqli_fetch_assoc($result)){
                array_push($data_mhs,$mhs);
            }
            echo json_encode($data_mhs);
        }
    }
?>